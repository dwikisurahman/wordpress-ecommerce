<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ecommercekreativ' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o)=J!>4ckkhnq.q!z%LL99,,_^/WOfq-<=M*>@EKfZYZ(Q:AJ[0xTN^0V#*ju/Ba' );
define( 'SECURE_AUTH_KEY',  ']W7K(rSUA:iRdqH98~)f,u]Jst%S)E`Y1ds!Z=u+fW?y1(3hYqxTDV^EdnPc%C +' );
define( 'LOGGED_IN_KEY',    'xvc@!xhQcP*gy&n9IxYG*asCqS7g^ER5|Sl+__e)(7{tq;-Ld{)F~Q;()c|;?B^=' );
define( 'NONCE_KEY',        'zlAOYmYwwsnqxfx*BE%R[CK6 ]nXuJu1q,6qrn&F(=3c[`AF>,,PK<RdLL|W8g5^' );
define( 'AUTH_SALT',        '<sx.js>y|5|t[jgH3D>kd/9(@}CMXz?70rKWF?e,u4tCy)4t4/-V$=!,{^Ms.VfG' );
define( 'SECURE_AUTH_SALT', ';#!P5-qUO8i/}zLsr3|Mg!&D`fpBamSs-[d3zO^c.6i%!h MJ]aN_`KDN]M]_>h$' );
define( 'LOGGED_IN_SALT',   'mAg<#am7>1dE}5[A>s}bp:Qk3#>)F`H}i)@o-CHM/Q@@]%Uf{kDs+4HOO[A)6q 4' );
define( 'NONCE_SALT',       '&`O7#:w0c^DM9MhHoPara8Z%,;PHF5X8NBvnThpb1r,E,rIDCpCg=U0ir8_n{)OV' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
