<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php
        wp_enqueue_style('road-fighter_stylesheet', get_template_directory_uri() . "/style.css", '', '', 'all');
        wp_head(); ?>
    </head>
    <body <?php body_class(); ?> style="background:url('<?php
    if (roadfighter_get_option('roadfighter_bodybg') != '') {
        echo roadfighter_get_option('roadfighter_bodybg');
    } else {
        echo '';
    }
    ?>');">
        <div class="header_container <?php if (is_home()) {
            echo 'home';
        } else {
            echo 'not_home';
        }
            ?>
             ">
            <div class="container_24">
                <div class="grid_24">
                    <header class="header <?php if (is_front_page()) { ?>home <?php
                    } else {
                        echo 'not_home';
                    }
                    ?>">
                        <div class="clear"></div>
                            <div id="MainNav">
								<span style="background-color:#99FF00;"><?php roadfighter_nav(); ?> </span>
                            </div>   
                    </header>
                </div>
                <div class="clear"></div>
            </div>
        </div>